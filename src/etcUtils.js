const { createElementInfo } = require('./renderUtils');

function createGroup(children, id, props = {}) {
    return createElementInfo('g', { ...props, id }, children);
}

module.exports = {
    group: createGroup,
    pt: (x, y) => ({ x, y }),
    isPt: pt => {
        return pt.x !== undefined && pt.y !== undefined;
    }

}
